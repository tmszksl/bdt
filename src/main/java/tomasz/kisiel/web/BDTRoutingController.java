package tomasz.kisiel.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import tomasz.kisiel.model.Route;
import tomasz.kisiel.model.RoutingService;

@RestController
public class BDTRoutingController {

	@Autowired
	RoutingService routingService;

	@GetMapping("/routing/{origin}/{destination}")
	public ResponseEntity<Route> routing(@PathVariable final String origin, @PathVariable final String destination) {
		final String[] route = routingService.find(origin, destination);
		return route == null
			? ResponseEntity.badRequest().build()
			: ResponseEntity.ok(new Route(route));
	}

}
