package tomasz.kisiel.model;

public final class Route {

    public final String[] route;

    public Route(final String[] route) {
        this.route = route;
    }

}
