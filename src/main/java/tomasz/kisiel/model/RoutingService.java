package tomasz.kisiel.model;

public interface RoutingService {
    String[] find(String origin, String destination);
}
