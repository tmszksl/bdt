package tomasz.kisiel.model;

import java.util.Map;

public interface CountryService {
    Map<String, Country> load();
}
