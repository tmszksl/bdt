package tomasz.kisiel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BDTApplication {

	public static void main(final String[] args) {
		SpringApplication.run(BDTApplication.class, args);
	}

}
