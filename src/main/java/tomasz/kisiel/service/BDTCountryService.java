package tomasz.kisiel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tomasz.kisiel.model.Country;
import tomasz.kisiel.model.CountryService;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Service("countryService")
public class BDTCountryService implements CountryService {

    @Value("${backend.developer.test.countries.json}")
    String jsonUrl;

    @Autowired
    RestTemplate restTemplate;

    @Override
    @Cacheable("countries")
    public Map<String, Country> load() {
        final Country[] countries = restTemplate.getForObject(jsonUrl, Country[].class);
        return Arrays.stream(countries).collect(Collectors.toMap(
            (country) -> country.cca3,
            (country) -> country)
        );
    }

}
