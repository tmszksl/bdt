package tomasz.kisiel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tomasz.kisiel.model.Country;
import tomasz.kisiel.model.CountryService;
import tomasz.kisiel.model.RoutingService;

import java.util.*;
import java.util.stream.Collectors;

@Service("routingService")
public class BDTRoutingService implements RoutingService {

    @Autowired
    CountryService countryService;

    @Override
    public String[] find(final String origin, final String destination) {
        final Map<String, Country> countries = countryService.load();
        if (countries.containsKey(origin) && countries.containsKey(destination)) {
            return new Routing(countries, origin, destination).find();
        } else {
            return null;
        }
    }

    static class Routing {

        final Set<String> visited = new HashSet<>();
        final Map<String, Country> countries;
        final String origin;
        final String destination;

        Routing(final Map<String, Country> countries, final String origin, final String destination) {
            this.countries = countries;
            this.origin = origin;
            this.destination = destination;
        }

        public String[] find() {
            Set<String[]> routes = Collections.singleton(new String[] { origin });
            while (true) {
                routes = nextSteps(routes);
                if (routes.isEmpty()) {
                    return null;
                }
                for (final String[] route : routes) {
                    final String last = route[route.length - 1];
                    if (last.equals(destination)) {
                        return route;
                    }
                }
            }
        }

        Set<String[]> nextSteps(final Set<String[]> currentSteps) {
            return currentSteps.stream()
                .map((route) -> {
                    final String last = route[route.length - 1];
                    visited.add(last);
                    final Country country = countries.get(last);
                    return Arrays.stream(country.borders)
                        .filter((c) -> !visited.contains(c) && countries.containsKey(c))
                        .map((c) -> copy(route, c))
                        .collect(Collectors.toList());
                })
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        }

        String[] copy(final String[] array, final String element) {
            final String[] copy = new String[array.length + 1];
            System.arraycopy(array, 0, copy, 0, array.length);
            copy[array.length] = element;
            return copy;
        }

    }

}
