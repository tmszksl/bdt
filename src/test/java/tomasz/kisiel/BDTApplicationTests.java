package tomasz.kisiel;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.client.MockClientHttpRequest;
import org.springframework.mock.http.client.MockClientHttpResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;
import tomasz.kisiel.model.Route;
import tomasz.kisiel.service.BDTCountryService;
import tomasz.kisiel.service.BDTRoutingService;
import tomasz.kisiel.web.BDTRoutingController;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = { BDTRoutingController.class, BDTRoutingService.class, BDTCountryService.class, BDTApplicationTests.class })
class BDTApplicationTests {

	@Bean
	RestTemplate restTemplate() {
		return new RestTemplateStub("src/test/resources/countries.json");
	}

	@Autowired
	BeanFactory context;

	@Test
	void contextLoads() {
		BDTRoutingController controller = context.getBean(BDTRoutingController.class);
		ResponseEntity<Route> response = controller.routing("CZE", "ITA");
		Route body = response.getBody();
		assertThat(body.route).contains("CZE", "AUT", "ITA");
	}

}

class RestTemplateStub extends RestTemplate {

	private final Path jsonFile;

	public RestTemplateStub(String jsonFile) {
		this.jsonFile = Path.of(jsonFile);

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
		setMessageConverters(Arrays.asList(converter));
	}

	@Override
	protected ClientHttpRequest createRequest(URI url, HttpMethod method) throws IOException {
		return new MockClientHttpRequest(HttpMethod.GET, url) {
			InputStream testFile() throws IOException {
				return Files.newInputStream(jsonFile);
			}
			@Override
			protected ClientHttpResponse executeInternal() throws IOException {
				return new MockClientHttpResponse(testFile(), 200);
			}
		};
	}
}
