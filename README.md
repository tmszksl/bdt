# BackendDeveloperTest

## Prerequisites

 * Java 11

## Test

```sh
./mvnw clean test
```

## Run

Configuration: `src/main/resources/application.properties`

```sh
./mvnw exec:java -Dexec.mainClass=tomasz.kisiel.BDTApplication -Dserver.port=8181
```

REST call:

```sh
curl -v http://localhost:8181/routing/CZE/ITA
curl -v http://localhost:8181/routing/GRC/PRT
```
